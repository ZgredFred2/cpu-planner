# CPU Planner

## Description

Program generates statistics for cpu [scheduling strategies](https://www.guru99.com/cpu-scheduling-algorithms.html).
Implemented strategies:

- FCFS
- SJF
- SRT
- PS
- Round Robin (parametrized)

Generated statistics:

- Average Turnaround time
- Average Waiting time

## Input

```
First line: <n - number of processes that will be added>
Next n lines: <process entry time> <process burst time>
Next line: <m - number of round robin parameters>
Next line: <list of m round robin parameters>
```

## Usage

Compile from root folder:

```bash
javac src/**/*.java -d outFolder
```

Then run with input file or stdin

```bash
# with input file
java -classpath outFolder Main src/input/in1
# with stdin
java -classpath outFolder Main
```

Sample input:

```
5
0 10
0 29
0 3
0 7
0 12
2
1 10
```

Sample output:

```
Strategia: FCFS
[1 0 10.00][2 0 39.00][3 0 42.00][4 0 49.00][5 0 61.00] #[Process ID, entry time, complete time]
Średni czas obrotu: 40.20                               # Average turnaround time
Średni czas oczekiwania: 28.00                          # Average waiting time

Strategia: SJF
[3 0 3.00][4 0 10.00][1 0 20.00][5 0 32.00][2 0 61.00]
Średni czas obrotu: 25.20
Średni czas oczekiwania: 13.00

Strategia: SRT
[3 0 3.00][4 0 10.00][1 0 20.00][5 0 32.00][2 0 61.00]
Średni czas obrotu: 25.20
Średni czas oczekiwania: 13.00

Strategia: PS
[3 0 15.00][4 0 31.00][1 0 40.00][5 0 44.00][2 0 61.00]
Średni czas obrotu: 38.20
Średni czas oczekiwania: 0.00

Strategia: RR-1
[3 0 13.00][4 0 30.00][1 0 38.00][5 0 44.00][2 0 61.00]
Średni czas obrotu: 37.20
Średni czas oczekiwania: 25.00

Strategia: RR-10
[1 0 10.00][3 0 23.00][4 0 30.00][5 0 52.00][2 0 61.00]
Średni czas obrotu: 35.20
Średni czas oczekiwania: 23.00
```
