package Reader;

import Process.MyProcess;
import Utils.MyExeption;
import Utils.Pair;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MyReader {
  public static MyData parse(Scanner in) throws MyExeption {
    MyData result = new MyData();
    int lineNumber = 1;
    int id = 1;
    if (!in.hasNextLine()) {
      throw new MyExeption(lineNumber, "pusta linia");
    }
    int header = parseHeader(in.nextLine(), lineNumber++);
    result.processNumber = header;
    for (int i = 0; i < header; i++) {
      Pair processOrder = parseOrder(in.nextLine(), lineNumber++);
      result.getAt(processOrder.a).add(new MyProcess(processOrder, id++));
    }
    if (!in.hasNextLine()) {
      throw new MyExeption(lineNumber, "pusta linia");
    }
    int RR_number = parseHeader(in.nextLine(), lineNumber++);
    result.RR_number = RR_number;
    if (!in.hasNextLine()) {
      throw new MyExeption(lineNumber, "pusta linia");
    }
    List<Integer> RR_parameters =
        parseRRParamerers(in.nextLine(), RR_number, lineNumber++);
    result.getRR_parameters().addAll(RR_parameters);
    if (in.hasNext()) {
      throw new MyExeption(lineNumber, "za dużo danych");
    }
    return result;
  }

  private static int parseHeader(String s, int lineNumber) throws MyExeption {
    Scanner in = new Scanner(s);
    if (in.hasNextInt()) {
      int result = in.nextInt();
      if (in.hasNext()) {
        System.out.println("duzo");
        throw new MyExeption(lineNumber, "za dużo danych");
      }
      return result;
    }
    throw new MyExeption(lineNumber, "za mało danych");
  }

  private static Pair parseOrder(String s, int lineNumber) throws MyExeption {
    Scanner in = new Scanner(s);
    int a;
    int b;
    if (in.hasNextInt()) {
      a = in.nextInt();
      if (in.hasNextInt()) {
        b = in.nextInt();
        if (in.hasNext()) {
          throw new MyExeption(lineNumber, "za dużo danych");
        }
        return new Pair(a, b);
      }
    }
    throw new MyExeption(lineNumber, "za mało danych");
  }

  private static List<Integer> parseRRParamerers(String s, int n,
                                                 int lineNumber)
      throws MyExeption {
    Scanner in = new Scanner(s);
    List<Integer> result = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      if (in.hasNextInt()) {
        int x = in.nextInt();
        result.add(x);
      } else {
        throw new MyExeption(lineNumber, "za mało danych");
      }
    }
    if (in.hasNext()) {
      throw new MyExeption(lineNumber, "za dużo danych");
    }
    return result;
  }
}
