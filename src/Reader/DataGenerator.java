package Reader;
import java.util.Scanner;

public class DataGenerator {
  public static Scanner getDefault() {
    String input = "5\n"
                   + "0 10\n"
                   + "0 29\n"
                   + "0 3\n"
                   + "0 7\n"
                   + "0 12\n"
                   + "2\n"
                   + "1 10";
    Scanner result = new Scanner(input);
    return result;
  }
  public static Scanner getCustom() {
    String input = "5\n"
                   + "0 10\n"
                   + "0 29\n"
                   + "1 3\n"
                   + "1 7\n"
                   + "10 12\n"
                   + "2\n"
                   + "1 10";
    Scanner result = new Scanner(input);
    return result;
  }

  public static Scanner getCustom2() {
    String input = "5\n"
                   + "0 10\n"
                   + "0 10\n"
                   + "1 9\n"
                   + "1 9\n"
                   + "10 12\n"
                   + "2\n"
                   + "1 10";
    Scanner result = new Scanner(input);
    return result;
  }

  public static Scanner getCustom3() {
    String input = "5\n"
                   + "0 11\n"
                   + "0 12\n"
                   + "0 13\n"
                   + "0 14\n"
                   + "40 1\n"
                   + "2\n"
                   + "1 10";
    Scanner result = new Scanner(input);
    return result;
  }

  public static Scanner getCustom4() {
    String input = "3\n"
                   + "0 1\n"
                   + "0 5\n"
                   + "4 1\n"
                   + "0\n"
                   + " ";
    Scanner result = new Scanner(input);
    return result;
  }

  public static Scanner getWa() {
    String input = "5\n"
                   + "0 10\n"
                   + "0 29\n"
                   + "0 3\n"
                   + "0\n"
                   + "0 12\n"
                   + "2\n"
                   + "1 10";
    Scanner result = new Scanner(input);
    return result;
  }

  public static Scanner getIn0() {
    String input = "10\n"
                   + "0 5\n"
                   + "8 13\n"
                   + "4 13\n"
                   + "7 15\n"
                   + "8 7\n"
                   + "11 13\n"
                   + "2 10\n"
                   + "9 15\n"
                   + "11 15\n"
                   + "6 10\n"
                   + "8\n"
                   + "10 6 6 7 9 6 9 8\n";
    Scanner result = new Scanner(input);
    return result;
  }

  public static Scanner getIn10() {
    String input = "12\n"
                   + "0 14\n"
                   + "0 16\n"
                   + "0 13\n"
                   + "5 18\n"
                   + "1 12\n"
                   + "12 11\n"
                   + "10 5\n"
                   + "8 13\n"
                   + "0 3\n"
                   + "14 10\n"
                   + "9 11\n"
                   + "9 5\n"
                   + "7\n"
                   + "9 7 16 12 3 20 9\n";
    Scanner result = new Scanner(input);
    return result;
  }
}
