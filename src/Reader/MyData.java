package Reader;
import Utils.Moment;
import java.util.*;

public class MyData {
  int processNumber;
  int RR_number;
  private Map<Integer, Moment> momenty = new TreeMap<Integer, Moment>();
  private List<Integer> RR_parameters = new ArrayList<>();

  public Moment getAt(int idx) {
    Moment res = getMomenty().get(idx);
    if (res == null) {
      res = new Moment();
      getMomenty().put(idx, res);
      return getAt(idx);
    }
    return res;
  }

  public Map<Integer, Moment> getMomenty() { return momenty; }

  public void setMomenty(Map<Integer, Moment> momenty) {
    this.momenty = momenty;
  }

  public List<Integer> getRR_parameters() { return RR_parameters; }

  public void setRR_parameters(List<Integer> rR_parameters) {
    this.RR_parameters = rR_parameters;
  }

  @Override
  public String toString() {
    String res = "";

    int momentsNumber = getMomenty().size();
    res += "Liczba procesow: " + processNumber + "\n";
    res += "liczba momentow: " + momentsNumber;
    res += getMomenty().toString() + "\n";
    res += "RR-ki: " + RR_number + "\n";
    res += getRR_parameters().toString();

    return res;
  }
}
