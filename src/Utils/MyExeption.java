package Utils;
public class MyExeption extends Throwable {
  int lineNumber;
  String message;

  public MyExeption(int lineNumber, String message) {
    this.lineNumber = lineNumber;
    this.message = message;
  }

  @Override
  public String toString() {
    return message + " w lini numer: " + lineNumber;
  }
}
