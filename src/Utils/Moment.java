package Utils;
import Process.MyProcess;
import java.util.ArrayList;
import java.util.List;

public class Moment {
  private List<MyProcess> processes = new ArrayList<>();
  public void add(MyProcess value) { getProcesses().add(value); }

  public List<MyProcess> getProcesses() { return processes; }

  public void setProcesses(List<MyProcess> processes) {
    this.processes = processes;
  }

  @Override
  public String toString() {
    return getProcesses().toString();
  }
}
