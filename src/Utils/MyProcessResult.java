package Utils;
import Process.MyProcess;
import java.util.Locale;

public class MyProcessResult {
  MyProcess process;
  double end;

  public MyProcessResult(MyProcess process, double end) {
    this.process = process;
    this.end = end;
  }

  @Override
  public String toString() {
    return String.format(Locale.US, "[%d %d %1.2f]", process.getId(),
                         process.getIncomeTime(), end);
  }
}
