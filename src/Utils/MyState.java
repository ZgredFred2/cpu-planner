package Utils;
public class MyState {
  MyProgress progress;
  private double timeAt;

  public MyState(MyProgress progress, double timeAt) {
    this.progress = progress;
    this.setTimeAt(timeAt);
  }

  public double getTimeAt() { return timeAt; }

  public void setTimeAt(double timeAt) { this.timeAt = timeAt; }
}
