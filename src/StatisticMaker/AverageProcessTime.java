package StatisticMaker;

import Process.MyProcess;

public class AverageProcessTime extends StatisticMaker {
  private double totalProcesses;
  private double totalProcessTime;
  boolean debug = false;
  public AverageProcessTime() {
    super();
    totalProcesses = 0;
    totalProcessTime = 0;
  }

  public void AddProcess(MyProcess process, double time) {
    ++totalProcesses;
    if (debug) {
      System.out.println("add " + process.toString() + " " + time);
    }
    super.AddProcess(process, time);
  }

  public void StartProcess(MyProcess process, double time) {
    if (debug) {
      System.out.println("start " + process + " " + time);
    }
    super.StartProcess(process, time);
  }

  public void FinishProcess(MyProcess process, double time) {
    if (debug) {
      System.out.println("finish " + process + " " + time);
    }
    double processTime = time - (double)process.getIncomeTime();
    totalProcessTime += processTime;
    super.FinishProcess(process, time);
  }

  public void MoveToWaitingProccess(MyProcess process, double time) {
    if (debug) {
      System.out.println("canceled " + process + " " + time);
    }
    super.MoveToWaitingProccess(process, time);
  }

  @Override
  public void Finish() {}

  @Override
  public String TypeToString() {
    return "Średni czas obrotu";
  }

  @Override
  public double Result() {
    return totalProcessTime / totalProcesses;
  }
}
