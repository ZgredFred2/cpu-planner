package StatisticMaker;
import Process.MyProcess;
import java.util.ArrayList;
import java.util.List;

public class StatisticReproducer {
  private List<StatisticMaker> statisticRaporters;
  public StatisticReproducer() {
    statisticRaporters = new ArrayList<>();
    statisticRaporters.add(new AverageProcessTime());
    statisticRaporters.add(new AverageWaitingTime());
  }

  public void AddProcess(MyProcess process, double time) {
    for (StatisticMaker statisticMaker : statisticRaporters) {
      statisticMaker.AddProcess(process, time);
    }
  }

  public void StartProcess(MyProcess process, double time) {
    for (StatisticMaker statisticMaker : statisticRaporters) {
      statisticMaker.StartProcess(process, time);
    }
  }

  public void FinishProcess(MyProcess process, double time) {
    for (StatisticMaker statisticMaker : statisticRaporters) {
      statisticMaker.FinishProcess(process, time);
    }
  }

  public void MoveToWaitingProccess(MyProcess process, double time) {
    for (StatisticMaker statisticMaker : statisticRaporters) {
      statisticMaker.MoveToWaitingProccess(process, time);
    }
  }

  public void Finish() {
    for (StatisticMaker statisticMaker : statisticRaporters) {
      statisticMaker.Finish();
    }
  }

  public void PrintResult() {
    for (StatisticMaker statisticMaker : statisticRaporters) {
      statisticMaker.PrintResult();
    }
  }
}
