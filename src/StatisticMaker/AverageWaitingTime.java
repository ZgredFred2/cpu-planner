package StatisticMaker;

import Process.MyProcess;

public class AverageWaitingTime extends StatisticMaker {
  private double totalProcesses;
  private double totalWaitingTime;

  public AverageWaitingTime() {
    super();
    totalProcesses = 0;
    totalWaitingTime = 0;
  }

  public void AddProcess(MyProcess process, double time) {
    ++totalProcesses;
    //        System.out.println("add " + process.toString() + " " + time);
    super.AddProcess(process, time);
  }

  public void StartProcess(MyProcess process, double time) {
    //        System.out.println("start " + process + " " + time);
    double waitingTime = time - map.get(process).getTimeAt();
    totalWaitingTime += waitingTime;
    super.StartProcess(process, time);
  }

  public void FinishProcess(MyProcess process, double time) {
    //        System.out.println("finish " + process + " " + time);
    super.FinishProcess(process, time);
  }

  public void MoveToWaitingProccess(MyProcess process, double time) {
    //        System.out.println("canceled " + process + " " + time);
    super.MoveToWaitingProccess(process, time);
  }

  @Override
  public void Finish() {}

  @Override
  public String TypeToString() {
    return "Średni czas oczekiwania";
  }

  @Override
  public double Result() {
    return totalWaitingTime / totalProcesses;
  }
}
