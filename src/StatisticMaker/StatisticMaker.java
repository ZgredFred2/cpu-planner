package StatisticMaker;
import Process.MyProcess;
import Utils.MyProgress;
import Utils.MyState;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public abstract class StatisticMaker {
  Map<MyProcess, MyState> map;

  public StatisticMaker() { map = new TreeMap<>(); }

  public void AddProcess(MyProcess process, double time) {
    map.put(process, new MyState(MyProgress.WAITING, time));
  }

  public void StartProcess(MyProcess process, double time) {
    map.put(process, new MyState(MyProgress.IN_PROCESS, time));
  }

  public void FinishProcess(MyProcess process, double time) {
    map.put(process, new MyState(MyProgress.FINISHED, time));
  }

  public void MoveToWaitingProccess(MyProcess process, double time) {
    map.put(process, new MyState(MyProgress.WAITING, time));
  }

  public void Finish() {}

  public void PrintResult() {
    System.out.println(
        String.format(Locale.US, "%s: %1.2f", TypeToString(), Result()));
  }

  public abstract String TypeToString();

  public abstract double Result();
}
