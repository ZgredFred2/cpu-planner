package Scheduler;
import Planner.Planner;
import Process.MyProcess;
import java.util.Comparator;
import java.util.PriorityQueue;

public class SJF extends Planner {
  double timeToSpend;
  PriorityQueue<MyProcess> queue;
  double lastAction;

  public SJF() {
    queue = new PriorityQueue<>(processComperator);
    lastAction = 0;  // whatever
    timeToSpend = 0; // whatever
  }

  @Override
  public void AddProcess(MyProcess process, double time) {
    timeToSpend = time - lastAction;
    while (queue.peek() != null && timeToSpend > 0) {
      MyProcess poll = queue.poll();
      timeToSpend = Math.min(timeToSpend, time - poll.getIncomeTime());
      if (poll.getNeedTime() <= timeToSpend) {
        if (poll.isActivated() == false) {
          super.StartProcess(poll, time - timeToSpend);
        }
        timeToSpend -= poll.getNeedTime();
        super.FinishProcess(poll, time - timeToSpend);
      } else {
        if (poll.isActivated() == false) {
          poll.setActivated(true);
          super.StartProcess(poll, time - timeToSpend);
        }
        poll.setNeedTime(poll.getNeedTime() - timeToSpend);
        timeToSpend -= timeToSpend;
        queue.add(poll);
      }
    }
    timeToSpend = 0;

    if (queue.peek() == null)
      lastAction = time;

    lastAction = time - timeToSpend;
    if (process != null) {
      queue.add(process);
    }
  }

  @Override
  public void Finish() {
    AddProcess(null, 1000000.0f);
  }

  @Override
  public String toString() {
    return "SJF";
  }
}
