package Scheduler;
import Planner.Planner;
import Process.MyProcess;
import java.util.PriorityQueue;

public class SRT extends Planner {
  double timeToSpend;
  PriorityQueue<MyProcess> queue;
  double lastAction;

  public SRT() {
    queue = new PriorityQueue<>(processComperator);
    lastAction = 0;
    timeToSpend = 0;
  }

  public void Dupa(MyProcess process, double time) {
    timeToSpend = time - lastAction;
    while (queue.peek() != null && queue.peek().getNeedTime() <= timeToSpend) {
      MyProcess poll = queue.poll();
      if (!poll.isActivated()) {
        poll.setActivated(true);
        super.StartProcess(poll, time - timeToSpend);
      }
      timeToSpend -= poll.getNeedTime();
      super.FinishProcess(poll, time - timeToSpend);
    }
    // add item
    if (queue.peek() != null) {
      double remainingTime = queue.peek().getNeedTime() - timeToSpend;
      if (process != null) {
        if (remainingTime > process.getNeedTime()) {
          MyProcess poll = queue.poll();
          if (poll.isActivated()) {
            poll.setActivated(false);
          } else {
            super.StartProcess(poll, time - timeToSpend);
          }
          poll.setNeedTime(poll.getNeedTime() - timeToSpend);
          timeToSpend -= timeToSpend;
          super.MoveToWaitingProccesses(poll, time - timeToSpend);
          queue.add(poll);
        }
      }
    }
    // activate top element
    if (queue.peek() != null) {
      MyProcess poll = queue.poll();
      if (poll.isActivated() == false) {
        super.StartProcess(poll, time - timeToSpend);
        poll.setActivated(true);
      }
      queue.add(poll);
    }

    lastAction = time - timeToSpend;
    if (process != null) {
      queue.add(process);
    }
  }

  @Override
  public void AddProcess(MyProcess process, double time) {
    timeToSpend = time - lastAction;
    while (queue.peek() != null && timeToSpend > 0) {
      MyProcess poll = queue.poll();
      timeToSpend = Math.min(timeToSpend, time - poll.getIncomeTime());
      if (poll.getNeedTime() <= timeToSpend) {
        if (poll.isActivated() == false) {
          super.StartProcess(poll, time - timeToSpend);
        }
        timeToSpend -= poll.getNeedTime();
        super.FinishProcess(poll, time - timeToSpend);
      } else {
        if (poll.isActivated() == false) {
          poll.setActivated(true);
          super.StartProcess(poll, time - timeToSpend);
        }
        poll.setNeedTime(poll.getNeedTime() - timeToSpend);
        timeToSpend -= timeToSpend;
        queue.add(poll);
      }
    }
    assert (timeToSpend == 0);
    if (queue.peek() != null && process != null) {
      if (process.getNeedTime() < queue.peek().getNeedTime()) {
        MyProcess poll = queue.poll();
        if (poll.isActivated())
          super.MoveToWaitingProccesses(poll, time);
        poll.setActivated(false);
        process.setActivated(true);
        super.StartProcess(process, time);
        queue.add(poll);
      }
    }

    if (queue.peek() == null)
      lastAction = time;

    lastAction = time - timeToSpend;

    if (process != null) {
      queue.add(process);
    }
  }

  @Override
  public void Finish() {
    AddProcess(null, 1000000.0f);
  }

  @Override
  public String toString() {
    return "SRT";
  }
}
