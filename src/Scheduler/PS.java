package Scheduler;
import Planner.Planner;
import Process.MyProcess;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class PS extends Planner {
  double timeToSpend;
  PriorityQueue<MyProcess> queue;
  double lastAction;

  public PS() {
    queue = new PriorityQueue<>(processComperator);
    lastAction = 0;
    timeToSpend = 0;
  }

  public void AddProcess(MyProcess process, double time) {
    timeToSpend = time - lastAction;

    while (!queue.isEmpty() && timeToSpend > 0.0f) {
      List<MyProcess> processes = new ArrayList<>();
      double num = queue.size();
      double min = queue.peek().getNeedTime();
      if (timeToSpend < num * min) {
        min = timeToSpend / num;
        timeToSpend = 0.0f;
      } else {
        timeToSpend -= min * num;
      }
      while (!queue.isEmpty()) {
        MyProcess poll = queue.poll();
        poll.setNeedTime(poll.getNeedTime() - min);
        if (poll.getNeedTime() > 0.0f) {
          processes.add(poll);
        } else {
          super.FinishProcess(poll, time - timeToSpend);
        }
      }

      queue.addAll(processes);
    }
    lastAction = time;
    if (process != null) {
      process.setActivated(true);
      super.StartProcess(process, time);
      queue.add(process);
    }
  }

  @Override
  public void Finish() {
    AddProcess(null, 1000000.0f);
  }

  private MyProcess ReduceNeedTime(MyProcess process, double timeAmmount) {
    process.setNeedTime(process.getNeedTime() - timeAmmount);
    return process;
  }

  @Override
  public String toString() {
    return "PS";
  }
}
