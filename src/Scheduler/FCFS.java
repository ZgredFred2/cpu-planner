package Scheduler;
import Planner.Planner;
import Process.MyProcess;
import java.util.Comparator;
import java.util.PriorityQueue;

public class FCFS extends Planner {
  private PriorityQueue<MyProcess> queue;

  private static Comparator<MyProcess> processComperator =
      new Comparator<MyProcess>() {
        @Override
        public int compare(MyProcess o1, MyProcess o2) {
          if (o1.getIncomeTime() > o2.getIncomeTime())
            return 1;
          if (o2.getIncomeTime() > o1.getIncomeTime())
            return -1;
          if (o1.getId() > o2.getId())
            return 1;
          if (o2.getId() > o1.getId())
            return -1;
          return 0;
        }
      };

  public FCFS() { queue = new PriorityQueue<>(processComperator); }

  @Override
  public void AddProcess(MyProcess process, double time) {
    queue.add(process);
  }

  @Override
  public void Finish() {
    if (queue.peek() != null) {
      double pp = queue.peek().getIncomeTime();
      while (queue.peek() != null) {
        MyProcess process = queue.poll();
        pp = Math.max(pp, process.getIncomeTime());
        super.StartProcess(process, pp);
        pp += process.getNeedTime();
        super.FinishProcess(process, pp);
      }
    }
  }

  @Override
  public String toString() {
    return "FCFS";
  }
}
