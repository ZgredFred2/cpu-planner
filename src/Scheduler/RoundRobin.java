package Scheduler;
import Planner.Planner;
import Process.MyProcess;
import java.util.*;

public class RoundRobin extends Planner {
  double timeToSpend;
  Deque<MyProcess> queue;
  double lastAction;
  int q;

  public RoundRobin(int q) {
    queue = new LinkedList<>();
    lastAction = 0;
    timeToSpend = 0;
    this.q = q;
  }

  public void AddProcess(MyProcess process, double time) {
    timeToSpend = time - lastAction;

    while (
        !queue.isEmpty() &&
        (timeToSpend >= q || timeToSpend >= queue.peekFirst().getNeedTime())) {
      MyProcess first = queue.pollFirst();
      timeToSpend = Math.min(timeToSpend, time - first.getIncomeTime());
      super.StartProcess(first, time - timeToSpend);
      if (first.getNeedTime() <= q && first.getNeedTime() <= timeToSpend) {
        timeToSpend -= first.getNeedTime();
        super.FinishProcess(first, time - timeToSpend);
      } else {
        timeToSpend -= q;
        first.setNeedTime(first.getNeedTime() - q);
        queue.addLast(first);
        super.MoveToWaitingProccesses(first, time - timeToSpend);
      }
    }
    lastAction = time - timeToSpend;
    if (process != null) {
      queue.addLast(process);
    }
  }

  @Override
  public void Finish() {
    AddProcess(null, 1000000.0f);
  }

  @Override
  public String toString() {
    return "RR-" + q;
  }
}
