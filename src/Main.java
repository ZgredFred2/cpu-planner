import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import Planner.DataDispenser;
import Reader.DataGenerator;
import Reader.MyData;
import Reader.MyReader;
import Utils.MyExeption;

public class Main {

  public static void main(String[] args) {
    Scanner in;
    try {
      if (args.length != 0) {
        in = new Scanner(new File(args[0]));
      } else {
        in = new Scanner(System.in);

        // Do testowania:
        // in = DataGenerator.getDefault();
        // in = DataGenerator.getCustom();
        // in = DataGenerator.getCustom2();
        // in = DataGenerator.getCustom3();
        // in = DataGenerator.getCustom4();
      }
      try {
        MyData data = MyReader.parse(in);
        DataDispenser.Simulate(data);
      } catch (MyExeption myExeption) {
        System.out.println(myExeption);
      }
    } catch (FileNotFoundException e) {
      System.out.println("Plik z danymi nie jest dostępny.");
    }
  }
}
