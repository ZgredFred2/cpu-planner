package Planner;
import Reader.MyData;
import Utils.Moment;
import java.util.Map;

public class DataDispenser {

  public static void Simulate(MyData data) {
    PlannerManagerReproducer plannerManagerReproducer =
        new PlannerManagerReproducer(data.getRR_parameters());
    for (Map.Entry<Integer, Moment> pair : data.getMomenty().entrySet()) {
      plannerManagerReproducer.Reproduce(pair);
    }
    plannerManagerReproducer.Finish();
  }
}
