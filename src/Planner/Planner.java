package Planner;
import Process.InformationGatherer;
import Process.MyProcess;
import java.util.Comparator;

public abstract class Planner implements InformationGatherer {
  InformationGatherer statisticGatherer;
  public void attachStatisticGatherer(InformationGatherer informationGatherer) {
    statisticGatherer = informationGatherer;
  }

  public abstract void AddProcess(MyProcess process, double time);

  @Override
  public void StartProcess(MyProcess process, double time) {
    statisticGatherer.StartProcess(process, time);
  }

  @Override
  public void FinishProcess(MyProcess process, double time) {
    statisticGatherer.FinishProcess(process, time);
  }

  @Override
  public void MoveToWaitingProccesses(MyProcess process, double time) {
    statisticGatherer.MoveToWaitingProccesses(process, time);
  }

  public abstract void Finish();

  protected static Comparator<MyProcess> processComperator =
      new Comparator<MyProcess>() {
        @Override
        public int compare(MyProcess o1, MyProcess o2) {
          if (o1.isActivated() && !o2.isActivated())
            return -1;
          if (o2.isActivated() && !o1.isActivated())
            return 1;

          if (o1.getNeedTime() > o2.getNeedTime())
            return 1;
          if (o2.getNeedTime() > o1.getNeedTime())
            return -1;
          if (o1.getId() > o2.getId())
            return 1;
          if (o2.getId() > o1.getId())
            return -1;
          return 0;
        }
      };
}
