package Planner;
import Process.MyProcess;
import Scheduler.FCFS;
import Scheduler.PS;
import Scheduler.RoundRobin;
import Scheduler.SJF;
import Scheduler.SRT;
import Utils.Moment;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PlannerManagerReproducer {
  List<PlannerManager> plannerManagers = new ArrayList<>();
  public PlannerManagerReproducer(List<Integer> rr_parameters) {
    plannerManagers.add(new PlannerManager(new FCFS()));
    plannerManagers.add(new PlannerManager(new SJF()));
    plannerManagers.add(new PlannerManager(new SRT()));
    plannerManagers.add(new PlannerManager(new PS()));
    for (int rr : rr_parameters) {
      plannerManagers.add(new PlannerManager(new RoundRobin(rr)));
    }
  }

  public void Reproduce(Map.Entry<Integer, Moment> pair) {
    for (PlannerManager plannerManager : plannerManagers) {
      for (MyProcess process : pair.getValue().getProcesses()) {
        plannerManager.AddProcess(new MyProcess(process), pair.getKey());
      }
    }
  }

  public void Finish() {
    for (PlannerManager plannerManager : plannerManagers) {
      plannerManager.Finish();
      plannerManager.PrintResult();
      System.out.println();
    }
  }
}
