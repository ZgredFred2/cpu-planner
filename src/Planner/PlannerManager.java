package Planner;
import Process.InformationGatherer;
import Process.MyProcess;
import StatisticMaker.StatisticReproducer;
import Utils.MyProcessResult;
import java.util.ArrayList;
import java.util.List;

public class PlannerManager implements InformationGatherer {
  Planner planner;
  StatisticReproducer statisticReproducer;
  List<MyProcessResult> resultList;
  public PlannerManager(Planner planner) {
    this.planner = planner;
    this.planner.attachStatisticGatherer(this);
    statisticReproducer = new StatisticReproducer();
    resultList = new ArrayList<MyProcessResult>();
  }

  public void AddProcess(MyProcess process, double time) {
    statisticReproducer.AddProcess(process, time);
    planner.AddProcess(process, time);
  }

  public void StartProcess(MyProcess process, double time) {
    statisticReproducer.StartProcess(process, time);
  }

  public void FinishProcess(MyProcess process, double time) {
    statisticReproducer.FinishProcess(process, time);
    resultList.add(new MyProcessResult(process, time));
  }

  @Override
  public void MoveToWaitingProccesses(MyProcess process, double time) {
    statisticReproducer.MoveToWaitingProccess(process, time);
  }

  @Override
  public void Finish() {
    planner.Finish();
    statisticReproducer.Finish();
  }

  public void PrintResult() {
    System.out.println("Strategia: " + planner.toString());
    for (MyProcessResult res : resultList) {
      System.out.print(res.toString());
    }
    System.out.println();
    statisticReproducer.PrintResult();
  }
}
