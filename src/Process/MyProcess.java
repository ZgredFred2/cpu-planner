package Process;

import Utils.Pair;

public class MyProcess implements Comparable<MyProcess> {
  private int incomeTime;
  private double needTime;
  private int id;
  private boolean activated;
  public MyProcess(Pair processOrder, int i) {
    setIncomeTime(processOrder.a);
    setNeedTime(processOrder.b);
    setActivated(false);
    setId(i);
  }

  public boolean isActivated() { return activated; }

  public void setActivated(boolean activated) { this.activated = activated; }

  public double getNeedTime() { return needTime; }

  public void setNeedTime(double needTime) { this.needTime = needTime; }

  public int getId() { return id; }

  public void setId(int id) { this.id = id; }

  public int getIncomeTime() { return incomeTime; }

  public void setIncomeTime(int incomeTime) { this.incomeTime = incomeTime; }

  @Override
  public String toString() {
    return "{" + getId() + "," + getNeedTime() + "," + getIncomeTime() + "}";
  }

  @Override
  public int compareTo(MyProcess o) {
    return Integer.compare(this.getId(), o.getId());
  }

  public MyProcess(MyProcess clone) {
    this.setIncomeTime(clone.getIncomeTime());
    this.setNeedTime(clone.getNeedTime());
    this.setActivated(clone.isActivated());
    this.setId(clone.getId());
  }
}
