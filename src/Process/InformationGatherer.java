package Process;
public interface InformationGatherer {

  void AddProcess(MyProcess process, double time);

  void StartProcess(MyProcess process, double time);

  void FinishProcess(MyProcess process, double time);

  void MoveToWaitingProccesses(MyProcess process, double time);

  void Finish();
}
